# CEDA Trike
The purpose of this repository is to provide software tools and documentation for the mapping tricycle.

## Getting Started
The software it written in Python 3. Once the trike is turned on, it takes up to 3 - 5 minutes to boot up the system. The long booting process is caused by initialization of Neural Network, which has to be uploaded to GPU. Once the booting sequence is ready, the trike is accessible on https://192.168.0.150:5000. This interface will provide main operational menu.

access details via WIFI:
- name of access point: **CEDA_VOZITKO**
- password: **a123456b**


Once all necessary data from road mapping are collected, data can be downloaded via Download_data.py file located in this repository.

## Installing dependencies
The `requirements.txt` file should list all Python libraries that your notebooks
depend on, and they will be installed using:

```
pip install -r requirements.txt
```
## Running the downloader
```
python Download_data.py
```